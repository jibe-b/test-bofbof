#! /bin/bash

safety=0
for secret_file in .gitpassword .gitusername .gitemail;do
    safety=$((safety+$(cat .gitignore |grep -P ""$secret_file"$"|wc -l)))
done

remote=$(git remote -v)
repo_root_url=$(echo $remote | grep origin | grep push | cut -d "/" -f 3)
repo_username=$(echo $remote | grep origin | grep push | cut -d "/" -f 4)
repo_name=$(echo $remote | grep origin | grep push | cut -d "/" -f 5 | cut -d " " -f1)

if [[ $safety == 3 ]];then
    if [[ $(git remote -v | grep @ | wc -l) == 0 ]];then
        username=$(cat .gitusername)
        password=$(cat .gitpassword)
        email=$(cat .gitemail)

        git config --global user.name $username
        git config --global user.email $email
        git config --global credential.helper 'cache --timeout 7200'


        git remote set-url origin https://$username:$password@$repo_root_url/$repo_username/$repo_name

        branche_perso="version-de-"$username
        git checkout -b $branche_perso
        echo "/!\ si vous avez réalisé des modifications entre le lancement de Binder et maintenant sur votre branche perso, elles seront perdues"
        echo "vous êtes maintenant sur votre branche "$branche_perso "(mise à jour à partir de vos modifications)"
        git pull --rebase --force origin $branche_perso
    else
        echo "L'url du répo est déja configurée avec des crédentials /!\Todo : implémenter la mise à jour des crédentials"
    fi
else
    echo "/!\SAFETY ISSUE: git does not ignore credential files"
fi
